
#[derive(Eq, PartialEq, Debug, Clone, Hash)]
enum BddVertex {
    Terminal { val: bool },
    Variable { low: usize, high: usize, var: usize }
}



#[derive(Debug)]
struct CallStackFrame<ArgT> {
    arg: ArgT,
    return_addr: Option<[usize; 2]>,
    result: [Option<usize>; 2],
}



#[derive(Eq, Debug, Clone, Hash)]
pub struct Bdd {
    graph: Vec<BddVertex>,
    len: usize
}

impl Bdd {
    pub fn new() -> Self {
        Self::terminal(false)
    }

    /// Returns a new BDD given by the valid valuations.
    /// Example: vec!["01", "11", "00"] -> [a BDD of implication].
    pub fn from_table(table: &Vec<&str>) -> Self {
        if table.is_empty() { return Self::new(); }

        let num_vars = table.first().unwrap().len();
        let mut variables = Vec::new();
        let mut bdd_or = Self::terminal(false);

        for i in 0..num_vars { variables.push(Self::variable(i)); }
        for element in table {
            assert!(element.len() == num_vars, "Each table element must be equally long.");
            let mut bdd_and = Self::terminal(true);
            for (index, i) in element.chars().map(|c| c != '0').enumerate() {
                bdd_and = Self::and(&bdd_and, &Self::iff(&Self::terminal(i), &variables[index]));
            }
            bdd_or = &bdd_or | &bdd_and;
        }

        bdd_or
    }

    /// Returns a new BDD, consisting of a single
    /// variable (and the respective two terminals).
    pub fn terminal(val: bool) -> Self {
        let vertex = BddVertex::Terminal { val };

        Bdd { graph: vec![vertex], len: 0 }
    }

    pub fn root_index(&self) -> usize {
        self.graph.len() - 1
    }

    /// Returns a number of variables of the BDD
    pub fn len(&self) -> usize {
        self.len
    }

    /// Returns a number of true valuations.
    pub fn sat_count(&self) -> usize {
        use BddVertex::*;

        let diff = |from_var, to_index| {
            match self.graph[to_index] {
                Terminal { .. } => self.len - from_var,
                Variable { var, .. } => var - from_var
            }
        };
        let diffpow_minus = |from_var, to_index| 2u32.pow(diff(from_var, to_index) as u32 - 1) as usize;
        let diffpow = |from_var, to_index| 2u32.pow(diff(from_var, to_index) as u32) as usize;

        let mut stack = vec![CallStackFrame { arg: self.root_index(), return_addr: None, result: [None, None] }];

        let result = loop {
            let CallStackFrame { arg, result, return_addr } = *stack.last().unwrap();
            let result = match self.graph[arg] {
                Terminal { val } => Ok(val as usize),
                Variable { var, low, high } => if let [Some(low_res), Some(high_res)] = result {
                    Ok(diffpow_minus(var, high) * high_res + diffpow_minus(var, low) * low_res)
                } else {
                    Err((low, high))
                }
            };
            match result {
                Ok(result) => {
                    match return_addr {
                        None => break result,
                        Some([a, b]) => {
                            stack[a].result[b] = Some(result);
                            stack.pop();
                            continue;
                        }
                    }
                },
                Err((low, high)) => {
                    let this_addr = stack.len() - 1;
                    let new_call = |x, y| CallStackFrame {
                        arg: x, result: [None, None], return_addr: Some([this_addr, y])};
                    stack.push(new_call(high, 1));
                    stack.push(new_call(low, 0));
                }
            }
        };

        diffpow(0, self.root_index()) * result 
    }

    /// Returns a particular assignment of variables
    /// that will result in true, if such an assignment does exists.
    pub fn witness(&self) -> Option<Vec<bool>> {
        use BddVertex::*;
        let mut witness = vec![true; self.len];
        let mut true_index = None;
        for (index, element) in self.graph.iter().enumerate() {
            if let Terminal { val: true } = element {
                true_index = Some(index);
                break
            }
        };

        if let None = true_index { return None }
        
        let mut from_index = true_index.unwrap();

        for (index, element) in self.graph.iter().enumerate() {
            if let Variable { low, high, var } = *element {
                if low == from_index {
                    witness[var] = false;
                    from_index = index;
                }
                if high == from_index {
                    witness[var] = true;
                    from_index = index;
                }
            }
        }

        Some(witness)
    }

    pub fn variable(var: usize) -> Self {
        let variable_node = BddVertex::Variable { var, low: 0, high: 1 };
        let one = BddVertex::Terminal { val: true };
        let zero = BddVertex::Terminal { val: false };

        Bdd { graph: vec![zero, one, variable_node], len: var + 1 }
    }

    /// Returns true iff a given BDD is satisfiable.
    /// If it's not satisfiable, it's equivalent to `false`,
    /// so a simple equivalence check suffices.
    pub fn sat(x: &Self) -> bool {
        *x != Self::terminal(false)
    }

    /// Returns true iff a given BDD is a tautology.
    pub fn valid(x: &Self) -> bool {
        *x == Self::terminal(true)
    }

    /// If two BDDs are equivalent, their 
    /// data representation is identical, EXCEPT
    /// their number of variables can differ.
    pub fn eq(x: &Self, y: &Self) -> bool {
        x.graph == y.graph
    }

    pub fn not(x: &Self) -> Self {
        let mut graph = x.graph.clone();
        let mut encountered_terminals = 0;
        for mut vertex in &mut graph {
            if let &mut BddVertex::Terminal { val } = &mut vertex {
                *val = !*val;
                encountered_terminals += 1;
            }
            if encountered_terminals == 2 {
                break;
            }
        }

        Bdd { graph, len: x.len }
    }

    pub fn restrict(set: &Vec<Self>, restr: &Self) -> Vec<Self> {
        set.iter().map(|bdd| Self::and(&bdd, &restr)).collect()
    }

    pub fn implies(x: &Self, y: &Self) -> Self {
        Self::apply(x, y, &|a, b| !a || b)
    }

    pub fn xor(x: &Self, y: &Self) -> Self {
        Self::apply(x, y, &|a, b| a != b)
    }

    pub fn iff(x: &Self, y: &Self) -> Self {
        Self::apply(x, y, &|a, b| a == b)
    }

    pub fn or(x: &Self, y: &Self) -> Self {
        Self::apply(x, y, &|a, b| a || b)
    }

    pub fn and(x: &Self, y: &Self) -> Self {
        Self::apply(x, y, &|a, b| a && b)
    }

    pub fn and_not(x: &Self, y: &Self) -> Self {
        Self::apply(x, y, &|a, b| a && !b)
    }

    /// When applying some function to a terminal node and some other BDD vertex,
    /// result is known -- without expensive computation -- just by inspection
    /// of the function.
    fn fn_analysis(x: &BddVertex, y: &BddVertex, op: &dyn Fn(bool, bool) -> bool) -> Option<Self> {
        use BddVertex::*;

        let fn_result = match (x, y) {
            (Terminal { val }, Variable { .. }) => Some((op(*val, true), op(*val, false))),
            (Variable { .. }, Terminal { val }) => Some((op(true, *val), op(false, *val))),
            _ => None
        };

        match (x, y) {
            (Terminal { val: va }, Terminal { val: vb }) => Some(Self::terminal(op(*va, *vb))),
            (Terminal { .. }, Variable { .. }) | (Variable { .. }, Terminal { .. }) => {
                match fn_result {
                    Some((false, false)) => Some(Self::terminal(false)),
                    Some((true, true)) => Some(Self::terminal(true)),
                    _ => None
                }
            },
            _ => None
        }
    }

    /// Zips two vectors of BDDs with one of the functions above.
    pub fn zip(op: &dyn Fn(&Self, &Self) -> Self, x: &Vec<Self>, y: &Vec<Self>) -> Vec<Self> {
        x.iter().zip(y.iter()).map(|(x, y)| op(x, y)).collect()
    }

    /// Evaluates BDD on a particular variable value assignment.
    /// As the variables are numbers, an assignment is a map
    /// N -> Bool, represented as a vector of Boolean values.
    /// Because of this, it is reasonable to choose BDD variables
    /// successively, starting from zero.
    pub fn eval(x: &Self, assignments: &Vec<bool>) -> bool {
        use BddVertex::*;

        let mut current_index = x.graph.len() - 1;
        loop {
            match &x.graph[current_index] {
                Terminal { val } => return *val,
                Variable { low, high, var } => current_index = match assignments[*var] {
                    true => *high, false => *low
                }
            }
        }
    }

    /// Applies a binary boolean function on two BDDs. Implementation
    /// of the recursive algorithm using an explicit stack for better performance.
    fn apply(x: &Self, y: &Self, op: &dyn Fn(bool, bool) -> bool) -> Self {
        use std::collections::HashMap;
        use std::cmp::Ordering::*;
        use BddVertex::*;

        let mut map = HashMap::<(usize, usize), usize>::new();
        let mut done = HashMap::<BddVertex, usize>::new();
        let mut graph = Vec::new();
        let base_call = CallStackFrame { arg: (x.root_index(), y.root_index()), return_addr: None, result: [None, None] };
        let mut stack = vec![base_call];

        let vertices = |arg: (usize, usize)| (&x.graph[arg.0], &y.graph[arg.1]);
        let returner = |[a, b]: [usize; 2], x, s: &mut Vec<CallStackFrame<(usize, usize)>>| s[a].result[b] = x;

        loop {
            let top = stack.last().unwrap();
            let CallStackFrame { arg, return_addr, .. } = *top;

            // result for the current stack frame is already computed
            if let Some(value) = map.get(&arg) {
                match return_addr {
                    None => break,
                    Some(address) => {
                        returner(address, Some(*value), &mut stack);
                        stack.pop();
                        continue
                    }
                }
            }

            // stack frame has got its required computational results from recursive calls:
            if let CallStackFrame { result: [Some(low), Some(high)], return_addr, arg } = *top {
                let mut result_index = None;
                let new = if let (Terminal { val: va }, Terminal { val: vb }) = vertices(arg) {
                    Terminal { val: op(*va, *vb) }
                } else {
                    let var = match vertices(arg) {
                        (Terminal { .. }, Variable { var, .. }) | (Variable { var, .. }, Terminal { .. }) => *var,
                        (Variable { var: va, .. }, Variable { var: vb, .. }) => std::cmp::min(*va, *vb),
                        _ => panic!("Cannot happen.")
                    };
                    if low == high {
                        map.insert(arg, high);
                        result_index = Some(high);
                    }
                    Variable { var, low, high }
                };
                if let Some(index) = done.get(&new) {
                    map.insert(arg, *index);
                    result_index = Some(*index);
                }
                if let None = result_index {
                    result_index = Some(graph.len());
                    graph.push(new.clone());
                    map.insert(arg, result_index.unwrap());
                }
                done.insert(new, result_index.unwrap());

                match return_addr {
                    None => break,
                    Some(address) => {
                        returner(address, result_index, &mut stack);
                        stack.pop();
                    }
                }
            }

            // base case for two terminals; always returns without recursive call:
            let CallStackFrame { arg, result, .. } = stack.last_mut().unwrap();
            if let (Terminal { .. }, Terminal { .. }) = vertices(*arg) {
                *result = [Some(0), Some(0)];
                continue
            }

            // recursive calls can be optimized away, because the result can be deduced
            let v = vertices(*arg);
            if let Some(_terminal_node) = Bdd::fn_analysis(v.0, v.1, op) {
                //println!("can be computed"); // !!
            }

            // recursive calls:
            let current = stack.len() - 1;
            let CallStackFrame { arg, .. } = *stack.last().unwrap();
            let (arg_l, arg_h) = match vertices(arg) {
                (Terminal { .. }, Variable { high, low, .. }) => ((arg.0, *low), (arg.0, *high)),
                (Variable { high, low, .. }, Terminal { .. }) => ((*low, arg.1), (*high, arg.1)),
                (Variable { var: va, high: ha, low: la }, Variable { var: vb, high: hb, low: lb }) => {
                    match va.cmp(&vb) {
                        Equal => ((*la, *lb), (*ha, *hb)),
                        Less => ((*la, arg.1), (*ha, arg.1)),
                        Greater => ((arg.0, *lb), (arg.0, *hb))
                    }
                },
                _ => panic!("Cannot happen.")
            };

            for (i, arg) in [arg_l, arg_h].iter().enumerate().rev() {
                stack.push(CallStackFrame { return_addr: Some([current, i]), arg: *arg, result: [None, None] });
            }
        }

        Bdd { graph, len: std::cmp::max(x.len, y.len) }
    }

    pub fn to_dot(&self) -> String {
        self.to_dot_with_names(&Vec::new())
    }

    /// Returns a graph in the DOT format. As the variables 
    /// are numbers, their respective nodes will be labeled as
    /// such, but only if the `N -> String` mapping `names` is empty,
    /// otherwise the mapping is used to label the vertices.
    pub fn to_dot_with_names(&self, names: &Vec<String>) -> String {
        use BddVertex::*;
        let mut result = String::from("digraph bdd {\n");
        let name = |i: usize| if names.is_empty() { format!("x{}", i) } else { names[i].clone() };

        for (index, vertex) in self.graph.iter().enumerate() {
            match vertex {
                Terminal { val } => result += &format!("{} [shape=box, label={}]\n", index, val),
                Variable { high, low, var } => result +=
                    &format!("{0} [label={1}]\n{0} -> {2} [style=dashed]\n{0} -> {3} [style=normal]\n",
                             index, name(*var), low, high)
            }
        }

        result + "}"
    }
}

impl std::ops::BitAnd for &Bdd {
    type Output = Bdd;

    fn bitand(self, other: Self) -> Self::Output {
        Bdd::and(self, other)
    }
}

impl std::ops::Not for &Bdd {
    type Output = Bdd;

    fn not(self) -> Self::Output {
        Bdd::not(self)
    }
}

impl std::ops::BitOr for &Bdd {
    type Output = Bdd;

    fn bitor(self, other: Self) -> Self::Output {
        Bdd::or(self, other)
    }
}

impl PartialEq for Bdd {
    fn eq(&self, other: &Self) -> bool {
        Bdd::eq(self, other)
    }
}



#[allow(unused_macros)]
macro_rules! bdd {
    // returns bdd
    ($a: ident && $b: ident) => { Bdd::and(&$a, &$b) };
    ($a: ident || $b: ident) => { Bdd::or(&$a, &$b) };
    ($a: ident => $b: ident) => { Bdd::implies(&$a, &$b) };
    ($a: ident - $b: ident)  => { Bdd::and_not(&$a, &$b) };
    (!$a: ident)             => { Bdd::not(&$a) };
    ($a: ident)              => { $a };
    // returns bool
    ($a: ident <=> $b: ident) => { Bdd::eq(&$a, &$b) };
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn terminal_and_syntactic() {
        let tfalse = Bdd::terminal(false);
        let ttrue = Bdd::terminal(true);
        let con = &tfalse & &ttrue;

        assert!(con == tfalse);
    }

    #[test]
    fn basic_not_syntactic() {
        let tfalse = Bdd::terminal(false);
        let ttrue = Bdd::terminal(true);
        let not_false = bdd!(!tfalse);

        assert!(ttrue == not_false);
    }

    #[test]
    fn eval_not() {
        let a = Bdd::variable(0);
        let assignments = vec![true];
        let b = !&a;

        assert!(Bdd::eval(&a, &assignments));
        assert!(!Bdd::eval(&b, &assignments));
    }

    #[test]
    fn implies_semantic() {
        let a = Bdd::variable(0);
        let b = Bdd::variable(1);
        let a_impl_b = Bdd::implies(&a, &b);

        let true_assignment1 = vec![true, true];
        let true_assignment2 = vec![false, false];
        let true_assignment3 = vec![false, true];
        let false_assignment1 = vec![true, false];

        assert!(Bdd::eval(&a_impl_b, &true_assignment1));
        assert!(Bdd::eval(&a_impl_b, &true_assignment2));
        assert!(Bdd::eval(&a_impl_b, &true_assignment3));
        assert!(!Bdd::eval(&a_impl_b, &false_assignment1));
    }

    #[test]
    fn longer_function_semantic() {
        let boolean = vec![true, false];

        let a = Bdd::variable(0);
        let b = Bdd::variable(1);
        let c = Bdd::variable(2);
        let d = Bdd::variable(3);

        let f = Bdd::implies(&Bdd::and(&a, &b), &Bdd::or(&c, &d));

        for x in &boolean { for y in &boolean { for z in &boolean { for t in &boolean {
            let assignment = vec![*x, *y, *z, *t]; 
            if *x && *y && !*z && !*t {
                assert!(!Bdd::eval(&f, &assignment))
            } else {
                assert!(Bdd::eval(&f, &assignment))
            }
        }}}}
    }

    #[test]
    fn two_vars_and_syntactic() {
        let a = Bdd::variable(0);
        let b = Bdd::variable(0);
        let a_and_b = Bdd::and(&a, &b);
        let assignments = vec![true];

        assert!(Bdd::eval(&a, &assignments));
        assert!(Bdd::eq(&a, &b));
        assert!(Bdd::eq(&b, &a_and_b))
    }

    #[test]
    fn a_and_not_a_syntactic() {
        let a = Bdd::variable(0);
        let not_a = Bdd::not(&a);
        let a_and_not_a = Bdd::and(&a, &not_a);
        let falsum = Bdd::terminal(false);
        
        assert_eq!(falsum, a_and_not_a)
    }

    #[test]
    fn simple_identities_syntactic() {
        let a = Bdd::variable(0);
        let ff = Bdd::terminal(false);
        let tt = Bdd::terminal(true);

        let false_and_a = &ff & &a;
        assert!(bdd!(false_and_a <=> ff));

        let false_or_a = &ff | &a;
        assert!(bdd!(false_or_a <=> a));

        let ex_falso_quodlibet = bdd!(ff => a);
        assert!(bdd!(ex_falso_quodlibet <=> tt));

        let negative_consequence = bdd!(a => ff);
        let not_a = bdd!(!a);
        assert!(bdd!(negative_consequence <=> not_a));

        let a_impl_a = bdd!(a => a);
        assert!(bdd!(a_impl_a <=> tt))
    }

    #[test]
    fn de_morgan_syntactic() {
        // !(a * b * !c) <=> (!a + !b + c)
        let a = Bdd::variable(0);
        let b = Bdd::variable(1);
        let c = Bdd::variable(2);

        let not_a = bdd!(!a);
        let not_b = bdd!(!b);
        let not_c = bdd!(!c);

        let mut first = bdd!(a && b);
        first = bdd!(first && not_c);
        first = bdd!(!first);
        let mut second = bdd!(not_a || not_b);
        second = bdd!(second || c);

        assert!(bdd!(first <=> second))
    }

    #[test]
    fn nontrivial_identity_syntactic() {
        // dnf (!a * !b * !c) + (!a * !b * c) + (!a * b * c) + (a * !b * c) + (a * b * !c)
        //                                    <=>
        // cnf            !(!a * b * !c) * !(a * !b * !c) * !(a * b * c)

        let a = Bdd::variable(0);
        let b = Bdd::variable(1);
        let c = Bdd::variable(2);
        let not_a = bdd!(!a);
        let not_b = bdd!(!b);
        let not_c = bdd!(!c);
        let mut nota_notb_notc = bdd!(not_a && not_b);
        nota_notb_notc = bdd!(nota_notb_notc && not_c);
        let mut nota_notb_c = bdd!(not_a && not_b);
        nota_notb_c = bdd!(nota_notb_c && c);
        let mut nota_b_c = bdd!(not_a && b);
        nota_b_c = bdd!(nota_b_c && c);
        let mut a_notb_c = bdd!(a && not_b);
        a_notb_c = bdd!(a_notb_c && c);
        let mut a_b_notc = bdd!(a && b);
        a_b_notc = bdd!(a_b_notc && not_c);
        let mut dnf = bdd!(nota_notb_notc || nota_notb_c);
        dnf = bdd!(dnf || nota_b_c);
        dnf = bdd!(dnf || a_notb_c);
        dnf = bdd!(dnf || a_b_notc);
        let mut nota_b_notc = bdd!(not_a && b);
        nota_b_notc = bdd!(nota_b_notc && not_c);
        let mut a_notb_notc = bdd!(a && not_b);
        a_notb_notc = bdd!(a_notb_notc && not_c);
        let mut a_b_c = bdd!(a && b);
        a_b_c = bdd!(a_b_c && c);
        let not_nota_b_notc = bdd!(!nota_b_notc);
        let not_a_notb_notc = bdd!(!a_notb_notc);
        let not_a_b_c = bdd!(!a_b_c);
        let mut cnf = bdd!(not_nota_b_notc && not_a_notb_notc);
        cnf = bdd!(cnf && not_a_b_c);
        
        assert!(bdd!(cnf <=> dnf))
    }

    #[test]
    fn unsat_semantic() {
        let a = Bdd::variable(0);
        let b = Bdd::variable(1);
        let not_a = bdd!(!a);
        let mut f = bdd!(a && b);
        f = bdd!(f && not_a);

        assert!(!Bdd::sat(&f))
    }

    #[test]
    fn sat_count_implication() {
        let bdd = Bdd::from_table(&vec!["00", "11", "01"]);
        
        assert_eq!(bdd.sat_count(), 3)
    }

    #[test]
    fn sat_count_dropped_variables() {
        // here the resulting BDD is equal to True, but it had 2 variables
        let first  = Bdd::from_table(&vec!["00", "01"]);
        let second = Bdd::from_table(&vec!["10", "11"]);

        let sum = &first | &second;

        println!("bdd is {:?}", sum.graph);
        println!("self len is {}", sum.len());

        assert!(Bdd::valid(&sum));
        assert_eq!(sum.sat_count(), 4)
    }
    
    #[test]
    fn sat_count_nothing_dropped() {
        let bdd = Bdd::from_table(&vec![
            "10110100",
            "11000010",
            "10010100",
            "10001100",
            "11110110",
        ]);

        println!("bdd is {:?}", bdd.graph);
        println!("bdd is {}", bdd.to_dot());
        
        assert_eq!(bdd.sat_count(), 5)
    }

    #[test]
    fn sat_count_dropped_first_and_last() {
        let vec = &vec![
            "11110",
            "11100",
            "10100",
            "01110",
            "01100",
            "00100",
            "11111",
            "11101",
            "10101",
            "01111",
            "01101",
            "00101",
        ];
        let bdd = Bdd::from_table(&vec);
        
        assert_eq!(bdd.sat_count(), vec.len())
    }

    #[test]
    fn sat_count_some_function() {
        let bdd = Bdd::from_table(&vec![
            "0001001010101",
            "0010100010100",
            "1110101010100",
            "0010101001111",
            "0011111111111"
        ]);
        assert_eq!(bdd.sat_count(), 5)
    }

    #[test]
    fn sat_count() {
        let bdd = Bdd::from_table(&vec![
            "00101",
                                        "11110",
                                        "10110",
                                        "11000"]);
        assert_eq!(bdd.sat_count(), 4);
        assert_eq!(Bdd::variable(0).sat_count(), 1);
    }

    #[test]
    fn sat_count_again() {
        let bdd = Bdd::from_table(&vec![
                                        "0100101011",
                                        "0111101110",
                                        "0110101110",
                                        "0111100010",
                                        "0100001011",
                                        "0111001110",
                                        "0110001110",
                                        "0111000010"
                                        ]);
        assert_eq!(bdd.sat_count(), 8)
    }

    #[test]
    fn sat_count_again_again() {
        let true_valuations = vec![
            "101010101010100100101",
            "001010101000000100101",
            "000010101000000100101",
            "110010101000000100101",
            "000010101000100101111",
            "100010101000100101111",
            "111110011111111111001",
            "111101111111111111001",
            "111011111110011111001",
            "110111111111011011000",
            "111111110011011011000"
        ];

        let bdd = Bdd::from_table(&true_valuations);

        assert!(bdd.sat_count() == true_valuations.len())
    }

    #[test]
    fn terminal_or() {
        let tfalse = Bdd::terminal(false);
        let ttrue = Bdd::terminal(true);
        let con = Bdd::or(&tfalse, &ttrue);

        assert!(bdd!(con <=> ttrue))
    }

    #[test]
    fn var_eval_semantic() {
        let va = Bdd::variable(0);
        let vb = Bdd::variable(1);

        assert!(Bdd::eval(&va, &vec![true, false]));
        assert!(!Bdd::eval(&vb, &vec![true, false]));
    }

    #[test]
    fn no_vars_eval_semantic() {
        let tfalse = Bdd::terminal(false);
        let ttrue = Bdd::terminal(true);

        assert!(!Bdd::eval(&tfalse, &vec![true, false]));
        assert!(Bdd::eval(&ttrue, &vec![true, false]));
    }
}
